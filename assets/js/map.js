// var m;
var base_url = window.location.origin;
var controller_url = base_url + '/bsts-commuter/index.php/maps/';

var map = L.map('map').setView([-6.9268, 107.6035], 13);

var station;
var stationmarker;
var stationLayer = L.layerGroup();

var trainmarker;
var trainLayer = L.layerGroup();

var stopsPopupContent = '<div></div>';


console.log(controller_url);

L.tileLayer('http://api.tiles.mapbox.com/v4/mapbox.light/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic3J1cGllZWUiLCJhIjoib05wWVBWTSJ9.RqbymEhEdLg2eDuVr6oPZg', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);




var stationIcon = L.icon({
  iconUrl: base_url + '/bsts-commuter/assets/img/marker-station.png',

  iconSize:     [28, 44], // size of the icon
  iconAnchor:   [14, 43], // point of the icon which will correspond to marker's location
  popupAnchor:  [0, -43] // point from which the popup should open relative to the iconAnchor
});


var trainIcon = L.icon({
  iconUrl: base_url + '/bsts-commuter/assets/img/marker-train.png',

  iconSize:     [30, 30], // size of the icon
  iconAnchor:   [14, 20] // point of the icon which will correspond to marker's location
  // popupAnchor:  [0, -43] // point from which the popup should open relative to the iconAnchor
});



onclick_stops();

function onclick_stops(){
  // stationLayer.clearLayer();
  $.post(controller_url + 'get_stops', function(stops) {
    // stops = $.parseJSON(stops);

    console.log(stops.length);

    for(var i=0; i < stops.length; i++){
      L.marker([stops[i].stop_lat, stops[i].stop_lon], {num: i, stop: stops[i], stop_id: stops[i].stop_id, icon: stationIcon})
        .bindPopup('<h3><a href="' + stops[i].stop_url + '"target="_blank">'+ stops[i].stop_name + '</a></h3>\
          <div>' + stops[i].stop_desc + '</div>\
          <h4>Jadwal Kereta</h4>\
          <ul class="span-jadwal"></ul>\
          ')
        .addTo(map).on('click', onStopsClick);
    }

  });
}

function onStopsClick(e) {
  console.log(this.options.stop_id);

  $.post(controller_url + 'get_stops_time/' + this.options.stop_id, function(stops_time) {
    // stops_time = $.parseJSON(stops_time);

    console.log(stops_time.length);
    // console.log(stops_time[0]);

    var span_jadwal = '';

    for(var i=0; i < stops_time.length; i++){
      console.log(stops_time[i].trip_id + '-' + stops_time[i].arrival_time + '-' + stops_time[i].departure_time + '-');
      span_jadwal = span_jadwal + '<li><span class="trip-id" data-trip-id="KA'+ stops_time[i].trip_id +'" >' + stops_time[i].trip_id + '</span> <span class="arrival-time">' + stops_time[i].arrival_time + '</span> <span class="departure-time">' + stops_time[i].departure_time + '</span> <span class="trip-status trip-on-time">On Time</span></li>';
    }

    // var a = m[this.options.num].getPopup();
    // var b = a._content.replace('<ul class="span-jadwal"></ul>","<ul class="span-jadwal">' + span_jadwal +'</ul>');
    // this.setPopupContent(b);
    $('.span-jadwal').html(span_jadwal);

    $('.trip-id').click(function(){
      console.log($(this).data("trip-id"));
      $.post(controller_url + 'get_train/' + $(this).data("trip-id"), function(train) {
          trainLayer.clearLayers();
          trainmarker = new L.marker([train.lat, train.long], {icon: trainIcon});
          trainLayer.addLayer(trainmarker);

          trainLayer.addTo(map);

          map.panTo(new L.LatLng(train.lat, train.long));

      });
    });

  });

  $.post(controller_url + 'get_delays', function(delays) {
    delays = $.parseJSON(delays);
    console.log(delays.length);
    for(var i=0; i < delays.length; i++){
      $('*[data-trip-id=' + delays[i].trip_id + ']').siblings('.trip-status').removeClass('trip-on-time').addClass('trip-delayed').html(Math.floor(delays[i].delay / 60) + " minutes");
      console.log(delays[i].trip_id + " - " + delays[i].delay);
    }
  });

}

$(document).ready(function(){
  $('.test').click(function(){
    $.post(controller_url + 'get_delays', function(delays) {
      delays = $.parseJSON(delays);
      console.log(delays.length);
      for(var i=0; i < delays.length; i++){
        // $('*[data-trip-id=' + delays[i].trip_id + ']').html('fdsaf');
        console.log(delays[i].trip_id + " - " + delays[i].delay);
      }
    });
  });
});
