<?php
class Stops_time_model extends CI_Model {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
  }

  function getData($stop_id) {
    //Query the data table for every record and row
    $this->db->where('stop_id', $stop_id);
    $this->db->where('departure_time >= NOW()');
    $this->db->order_by("arrival_time", "asc");
    //here we select every clolumn of the table
    $query = $this->db->get('stop_times', 4);

    if ($query->num_rows() == 0) {
      //show_error('Database is empty!');
    }else{
      return $query->result();
    }
  }

  function getArrivalTime($stop_id, $trip_id) {
    $trip_id = substr($trip_id, 2);
    //Query the data table for every record and row
    $this->db->select('arrival_time');
    $this->db->where('stop_id', $stop_id);
    $this->db->where('trip_id', $trip_id);
    //here we select every clolumn of the table
    $query = $this->db->get('stop_times');

    if ($query->num_rows() == 0) {
      //show_error('Database is empty!');
    }else{
      return $query->result();
    }
  }

  function getDepartureTime($stop_id, $trip_id) {
    $trip_id = substr($trip_id, 2);
    //Query the data table for every record and row
    $this->db->select('departure_time');
    $this->db->where('stop_id', $stop_id);
    $this->db->where('trip_id', $trip_id);
    //here we select every clolumn of the table
    $query = $this->db->get('stop_times');

    if ($query->num_rows() == 0) {
      //show_error('Database is empty!');
    }else{
      return $query->result();
    }
  }

}
?>