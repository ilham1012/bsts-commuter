<?php
class Maps extends CI_Controller{
  function get_stops() {
    //load model
    $this->load->model('stops_model');
    //add the header
    header('Content-Type: application/json');
    //encode json
    echo json_encode( $this->stops_model->getData() );
  }

  function get_stops_time($stop_id) {
    // $stop_id = 'ST_BD';
    //load model
    $this->load->model('stops_time_model');
    //add the header
    header('Content-Type: application/json');
    //encode json
    echo json_encode( $this->stops_time_model->getData($stop_id) );
  }

  function get_train($trip_id) {
    $data = json_decode(file_get_contents(asset_url() . 'js/posisikereta_sim.json'));

    // var_dump($data[0]->trip_id);

    //add the header
    header('Content-Type: application/json');

    for($i = 0; $i < sizeof($data); $i++){
      if($data[$i]->trip_id == $trip_id){
        echo json_encode($data[$i]);
      }
    }
  }

  function get_delays() {
    $data = json_decode(file_get_contents(asset_url() . 'js/posisikereta_sim.json'));
    $this->load->model('stops_model');
    $stops = $this->stops_model->getCoord();
    $this->load->model('stops_time_model');

    $prev_station        = 0;
    $next_station        = 0;
    $prev_next_distance  = 0;
    $train_next_distance = 0;

    $arrayJson = array();

    // var_dump($stops);


    for($i = 0; $i < sizeof($data); $i++){
      //get prev & next station
      if($data[$i]->dir_id == 0){
        // echo "==TUJUAN PADALARANG==";

        $found = false;

        $station = sizeof($stops) - 1;
        for($station; $station >= 0; $station--){
          // echo "stat " . $stops[$station]->stop_id . "//";
          if(($data[$i]->long > $stops[$station]->stop_lon)&&($found == false)){
            $found = true;
            $prev_station = $station + 1;//$stops[$station + 1]->stop_id;
            $next_station = $station;//$stops[$station]->stop_id;

            // echo "prev: " . $prev_station;
          }
        }
      }else if($data[$i]->dir_id == 1){
        // echo "==TUJUAN CICALENGKA==";

        $found = false;

        for($station = 1; $station < sizeof($stops); $station++){
          if(($data[$i]->long < $stops[$station]->stop_lon)&&($found == false)){
            $found = true;
            $prev_station = $station - 1;//$stops[$station - 1]->stop_id;
            $next_station = $station;//$stops[$station]->stop_id;

            // echo "prev: " . $prev_station;
          }
        }
      }


      //calculate prev to next station distance
      $prev_next_distance = $this->haversineGreatCircleDistance(
        $stops[$prev_station]->stop_lat, $stops[$prev_station]->stop_lon,
        $stops[$next_station]->stop_lat, $stops[$next_station]->stop_lon);
      // echo "prev_next_distance: " . $prev_next_distance;

      //calculate train to next station distance
      $train_next_distance = $this->haversineGreatCircleDistance(
        $data[$i]->lat, $data[$i]->long,
        $stops[$next_station]->stop_lat, $stops[$next_station]->stop_lon);
      // echo "train_next_distance: " . $train_next_distance;

      //calculate prev to next station time
      $prev_time = $this->stops_time_model->getDepartureTime(
          $stops[$prev_station]->stop_id,
          $data[$i]->trip_id);

      $next_time = $this->stops_time_model->getArrivalTime(
          $stops[$next_station]->stop_id,
          $data[$i]->trip_id);

      $prev_next_time = strtotime($next_time[0]->arrival_time) - strtotime($prev_time[0]->departure_time);
      // var_dump($this->stops_time_model->getDepartureTime(
      //     $stops[$next_station]->stop_id,
          // $data[$i]->trip_id));

      // echo " " . $next_time[0]->arrival_time . "-" . $prev_time[0]->departure_time . "time: " . " = " . $prev_next_time;
      // echo "time: " . $this->stops_time_model->getArrivalTime('ST_BD','KA410')->departure_time;//$stops[$next_station]->stop_id, $data[$i]->trip_id

      //calculate average scheduled speed
      //calculate ETA
      $train_next_time = $train_next_distance * $prev_next_time / $prev_next_distance;
      // echo " || train_next_time " . floor($train_next_time / 60);
      $tracking_time = date('H:i:s', strtotime($data[$i]->datetime));
      // echo "tracking_time " . $tracking_time;
      $eta = date('H:i:s', strtotime($tracking_time) + $train_next_time);
      // echo " // eta : " . $eta; //date('H:i:s', $eta);
      // echo " // sch : " . date('H:i:s', strtotime($next_time[0]->arrival_time));
      $delay = strtotime($eta) - strtotime($next_time[0]->arrival_time);
      // echo " id : " . $data[$i]->trip_id . " delay:: " . $delay;

      $arrayJson[] = array('trip_id' => $data[$i]->trip_id, 'delay' => $delay);
    }

    echo json_encode($arrayJson);
    // var_dump($data);
  }



  function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
    // echo "test";
    // $latitudeFrom = -6.949106;
    // $longitudeFrom = 107.712525;
    // $latitudeTo = -6.870046;
    // $longitudeTo = 107.517610;
    // $earthRadius = 6371000;


    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
      cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

    // echo "string" . ($angle * $earthRadius);
    return $angle * $earthRadius;
  }
}
?>