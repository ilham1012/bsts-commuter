<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?=$page_title?></title>

  <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.css" />
  <style media="screen" type="text/css">
    .span-jadwal{
      margin: 0;
      list-style: none;
      padding: 0;
    }

    .span-jadwal > li{
      padding: 5px;
      border-bottom: 1px solid #ccc;
      margin: 0;
    }

    .trip-id{
      font-weight: bold;
      margin-right: 20px;
      color: #0078A8;
      cursor: pointer;
    }

    .arrival-time, .departure-time{
      margin-right: 10px;
    }

    .trip-status.trip-on-time{
      color: green;
    }

    .trip-status.trip-delayed{
      color: red;
    }

  </style>

  <script type='text/javascript' src='<?=asset_url()?>js/jquery-2.1.4.min.js'></script>
  <script src="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.js"></script>
</head>

<body>
  <div id="map" style="height: 640px; border: 1px solid #AAA;"></div>
  <div class="test">click</div>

<!--  <script type='text/javascript' src='maps/markers.json'></script>-->
  <script type='text/javascript' src='<?=asset_url()?>js/map.js'></script>
</body>
</html>