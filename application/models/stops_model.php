<?php
class Stops_model extends CI_Model {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
  }

  function getData() {
    //Query the data table for every record and row
    $sql = 'SELECT * FROM stops';
    $query = $this->db->query($sql);

    if ($query->num_rows() == 0) {
      //show_error('Database is empty!');
    }else{
      return $query->result();
    }
  }

  function getCoord() {
    //Query the data table for every record and row
    $sql = 'SELECT stop_id, stop_lat, stop_lon FROM stops ORDER BY stop_lon ASC';
    $query = $this->db->query($sql);

    if ($query->num_rows() == 0) {
      //show_error('Database is empty!');
    }else{
      return $query->result();
    }
  }

}
?>